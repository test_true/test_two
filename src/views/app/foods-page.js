import React, { useEffect, useMemo, useRef, useState } from "react";
import { Row, Card, CardBody, CardTitle, Button } from "reactstrap";
import { Colxx } from "../../components/common/CustomBootstrap";
import CustomTable from "../../components/Table";
import ShortFilter from "../../components/table/ShortFilter";
import { connect } from "react-redux";
import { searchFoods } from "../../redux/foods/actions";
import { updateSelectFoods } from "../../redux/actions";
import { mergeArray, isEmpty } from "../../utils";
import { buildColumns, cellUtils } from "../../utils/table";
import foodsServices from "../../services/foodsServices";
import ResponseModal from "../../components/modal/ResponseModal";

const FoodsPage = ({ foods, user, history }) => {
  const modalRef = useRef();

  const [foodsData, setFoodsData] = useState(foods.rawData);
  const [selectFoodsData, setSelectFoodsData] = useState(foods.selected);
  const [totalSelectedData, setTotalSelectedData] = useState(mergeArray(foods.selected));

  useEffect(() => updateSelectFoods(selectFoodsData), [setSelectFoodsData]);
  useEffect(() => setTotalSelectedData(mergeArray(selectFoodsData)), [setTotalSelectedData]);
  useEffect(() => {
    if (!user) {
      history.push("/user/login");
    }
  }, []);

  const getFoods = (search = "") => {
    const results = [];
    search = search.toLowerCase();
    for (let i = 0; i < foods.rawData.length; i++) {
      if (foods.rawData[i]["description"].toLowerCase().indexOf(search) !== -1) {
        results.push(foods.rawData[i]);
      }
    }
    return results;
  };

  const buildDeleteActions = (value) => {
    return (
      <Button onClick={() => {
        let tmp = foods.selected;
        tmp.splice(value.row.index, 1);
        setTotalSelectedData(mergeArray(tmp));
        setSelectFoodsData([...tmp]);
        updateSelectFoods(selectFoodsData);
      }}>Delete</Button>);
  };
  const buildActions = (value) => {
    return (
      <Button onClick={() => {
        let tmp = foods.selected;
        tmp.push(value.row.original);
        setTotalSelectedData(mergeArray(tmp));
        setSelectFoodsData([...tmp]);
        updateSelectFoods(selectFoodsData);
      }}>Select</Button>);
  };

  const colsSelected = useMemo(() => [
    buildColumns("Description", "description", 40),
    buildColumns("Kcal", "kcal", 10, cellUtils.formatCurrency),
    buildColumns("Protein(g)", "protein_g", 10, cellUtils.formatCurrency),
    buildColumns("Fat(g)", "fa_mono_g", 10, cellUtils.formatCurrency),
    buildColumns("Carbs(g)", "carbohydrate_g", 10, cellUtils.formatCurrency),
    buildColumns("Action", "", 10, buildDeleteActions)
  ], []);
  const cols = useMemo(() => [
    buildColumns("Description", "description", 40),
    buildColumns("Kcal", "kcal", 10, cellUtils.formatCurrency),
    buildColumns("Protein(g)", "protein_g", 10, cellUtils.formatCurrency),
    buildColumns("Fat(g)", "fa_mono_g", 10, cellUtils.formatCurrency),
    buildColumns("Carbs(g)", "carbohydrate_g", 10, cellUtils.formatCurrency),
    buildColumns("Action", "", 10, buildActions)
  ], []);

  const onSubmitShortFilter = (search = "") => {
    setFoodsData(getFoods(search));
  };

  const onSendFoodsData = () => {
    foodsServices.sendFoods({ foods: selectFoodsData }).then(({ status, message }) => {
      if (status) {
        modalRef.current.success(message);
      }
    }).catch(() => {
      modalRef.current.fail("Fail");
    });
  };

  return (
    <>
      {user && <Row>
        <Colxx xxs="12">
          <Card className="mb-4">
            <CardBody>
              <CardTitle>
                <h3>User data</h3>
              </CardTitle>
              <p>Name: {user.fullname}</p>
              <p>Age: {user.age}</p>
            </CardBody>
          </Card>
        </Colxx>
      </Row>
      }
      <Row>
        <Colxx xxs="12">
          <Card className="mb-4">
            <CardBody>
              <CardTitle>
                <h3>Selected foods</h3>
              </CardTitle>
              <CustomTable
                columns={colsSelected}
                data={[...selectFoodsData]}
                noPage={true}
              />
              {!isEmpty(totalSelectedData) &&
              <table name="total_table" className={`r-table table`}>
                <thead>
                <tr>
                  <th className={"list-item-heading w-40 cell-table"}>
                    Total
                  </th>
                  <th className={"list-item-heading w-10 cell-table"}>
                    {totalSelectedData.kcal.toLocaleString()}
                  </th>
                  <th className={"list-item-heading w-10 cell-table"}>
                    {totalSelectedData.protein_g.toLocaleString()}
                  </th>
                  <th className={"list-item-heading w-10 cell-table"}>
                    {totalSelectedData.fa_mono_g.toLocaleString()}
                  </th>
                  <th className={"list-item-heading w-10 cell-table"}>
                    {totalSelectedData.carbohydrate_g.toLocaleString()}
                  </th>
                  <th className={"list-item-heading w-10 cell-table"}>
                    <Button onClick={onSendFoodsData}>Send</Button>
                  </th>
                </tr>
                </thead>
              </table>
              }
            </CardBody>
          </Card>
        </Colxx>
      </Row>

      <Row>
        <Colxx xxs="12">
          <Card className="mb-4">
            <CardBody>
              <CardTitle>
                <Colxx xxs={"12"}>
                  <ShortFilter onSubmit={onSubmitShortFilter} placeholder={[
                    "Description"
                  ]} />
                </Colxx>
              </CardTitle>
              <CustomTable
                columns={cols}
                data={[...foodsData]}
                paginationMaxSize={10}
              />
            </CardBody>
          </Card>
        </Colxx>
      </Row>
      <ResponseModal ref={modalRef} />
    </>
  );
};
const mapStateToProps = ({ foods, authUser }) => {
  const { user } = authUser;
  return { foods, user };
};

export default connect(mapStateToProps, {
  searchFoodsAction: searchFoods
})(FoodsPage);
