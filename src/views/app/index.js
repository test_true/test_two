import React, { Suspense } from "react";
import { Route, withRouter, Switch, Redirect } from "react-router-dom";
import AppLayout from "../../layout/AppLayout";
import { connect } from "react-redux";

const FoodsPage = React.lazy(() => import("./foods-page"));

const App = ({ match }) => {
  return (
    <AppLayout>
      <div className="dashboard-wrapper">
        <Suspense fallback={<div className="loading" />}>
          <Switch>
            <Redirect
              exact
              from={`${match.url}/`}
              to={`${match.url}/foods`}
            />
            <Route
              path={`${match.url}/foods`}
              render={(props) => <FoodsPage {...props} />}
            />
            <Redirect to="/user/login" />
          </Switch>
        </Suspense>
      </div>
    </AppLayout>
  );
};

export default withRouter(App);
