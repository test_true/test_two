import React, { useState, useEffect, useRef } from "react";
import { Row, Card, CardTitle, Label, FormGroup, Button } from "reactstrap";
import { connect } from "react-redux";
import { Formik, Form, Field } from "formik";
import { loginUser } from "../../redux/actions";
import { Colxx } from "../../components/common/CustomBootstrap";

import foodsServices from "../../services/foodsServices";
import ResponseModal from "../../components/modal/ResponseModal";

const validatePassword = (value) => {
  let error;
  if (!value) {
    error = "Please enter your password";
  } else if (value.length < 4) {
    error = "Value must be longer than 3 characters";
  }
  return error;
};

const validateEmail = (value) => {
  let error;
  if (!value) {
    error = "Please enter your email address";
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
    error = "Invalid email address";
  }
  return error;
};

const Login = ({ history, loginUserAction }) => {
  const modalRef = useRef();

  const [email] = useState("n.luengthongdee@gmail.com");
  const [password] = useState("password");

  const onUserLogin = (values) => {
    if (values.email !== "" && values.password !== "") {

      foodsServices.login(values.email, values.password).then((res) => {
        if(res.data.status) {
          loginUserAction(res.data.user, history);
        }
      }).catch((e) => {
        if(!e.status) {
          modalRef.current.fail(e.message);
        }
        else
        {
          modalRef.current.fail('Login fail.')
        }
      });
    }
  };

  const initialValues = { email, password };

  return (
    <Row className="h-100">
      <Colxx xxs="12" md="7" className="mx-auto my-auto">
        <Card className="auth-card">
          <div className="form-side">
            <h1>Test TRUE</h1>
            <CardTitle className="mb-4">
              Login
            </CardTitle>

            <Formik initialValues={initialValues} onSubmit={onUserLogin}>
              {({ errors, touched }) => (
                <Form className="av-tooltip tooltip-label-bottom">
                  <FormGroup className="form-group has-float-label">
                    <Label>
                      E-Mail
                    </Label>
                    <Field
                      className="form-control"
                      name="email"
                      validate={validateEmail}
                    />
                    {errors.email && touched.email && (
                      <div className="invalid-feedback d-block">
                        {errors.email}
                      </div>
                    )}
                  </FormGroup>
                  <FormGroup className="form-group has-float-label">
                    <Label>
                      Password
                    </Label>
                    <Field
                      className="form-control"
                      type="password"
                      name="password"
                      validate={validatePassword}
                    />
                    {errors.password && touched.password && (
                      <div className="invalid-feedback d-block">
                        {errors.password}
                      </div>
                    )}
                  </FormGroup>
                  <div className="d-flex justify-content-between align-items-center">

                    <Button
                      color="primary"
                      className={`btn-shadow btn-multiple-state`}
                      size="lg">
                      <span className="label">
                        Login
                      </span>
                    </Button>
                  </div>
                </Form>
              )}
            </Formik>
          </div>
        </Card>
      </Colxx>
      <ResponseModal ref={modalRef} />
    </Row>
  );
};

export default connect(null, {
  loginUserAction: loginUser
})(Login);
