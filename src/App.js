import React, { Suspense } from "react";
import { connect } from "react-redux";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";
import { NotificationContainer } from "./components/common/react-notifications";

const ViewApp = React.lazy(() => import("./views/app"));
const ViewUser = React.lazy(() => import("./views/user"));

class App extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="h-100">
        <>
          <NotificationContainer />
          <Suspense fallback={<div className="loading" />}>
            <Router>
              <Switch>
                <Route
                  path={"/app"}
                  component={ViewApp}
                />
                <Route
                  path="/user"
                  render={(props) => <ViewUser {...props} />}
                />
                <Redirect to="/user" />
              </Switch>
            </Router>
          </Suspense>
        </>
      </div>
    );
  }
}

const mapStateToProps = ({ authUser }) => {
  const { currentUser } = authUser;
  return { currentUser };
};
const mapActionsToProps = {};

export default connect(mapStateToProps, mapActionsToProps)(App);
