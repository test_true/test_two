import React, { useState } from "react";
import PropTypes from "prop-types";
import { Input } from "reactstrap";

import "./shortFilter.scss";

function ShortFilter({ onSubmit, placeholder }) {
  const [searchBox, setSearchBox] = useState("");

  const onInputChange = (event) => {
    const { value } = event.target;
    setSearchBox(value || "");
  };

  let placeholderText = "ค้นหาโดย ";
  if (Array.isArray(placeholder)) {
    placeholderText += placeholder.join(", ");
  } else {
    placeholderText = placeholder;
  }

  return (<form
    onSubmit={(e) => {
      e.preventDefault();
      onSubmit(searchBox);
    }} className="shortfilter-wrapper">
    <Input
      type="text"
      value={searchBox}
      name="searchBox"
      placeholder={placeholderText}
      onChange={onInputChange}
      className={`search-box`} />
    <button
      onClick={(e) => {
        e.preventDefault();
        onSubmit(searchBox);
      }}
      className={"btn btn-secondary"}
    >
      <i className="simple-icon-magnifier" />
    </button>
  </form>);
}

ShortFilter.defaultProps = {
  onSubmit: () => {
  }, placeholder: "Search ..."
};

ShortFilter.propTypes = {
  onSubmit: PropTypes.func,
  placeholder: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.array
  ])
};

export default ShortFilter;
