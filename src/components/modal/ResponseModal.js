import React from "react";
import { Button, Modal, ModalBody, ModalFooter } from "reactstrap";

export default class ResponseModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      isSuccess: false,
      label: ""
    };
  }

  success = (label, callback = () => this.setState({ open: false })) => this.setState({
    open: true,
    isSuccess: true,
    label,
    callback
  });

  fail = (label, callback = () => this.setState({ open: false })) => this.setState({
    open: true,
    isSuccess: false,
    label,
    callback
  });

  render() {
    const { onClose } = this.props;
    const { isSuccess, label } = this.state;
    return (<Modal
      isOpen={this.props.open || this.state.open}
      onClosed={onClose}
      centered
      contentClassName="content"
      size="sm"
    >
      <button
        type="button"
        className="close"
        aria-label="Close"
        style={{
          position: "absolute",
          top: 16,
          right: 20,
          zIndex: 1
        }}
        onClick={this.state.callback}
      >
        <span aria-hidden="true">×</span>
      </button>

      <ModalBody>
        <div className="align-items-center d-flex flex-column w-100">
          <i className={`iconsminds-${isSuccess ? "yes" : "close"} text-${isSuccess ? "primary" : "danger"}`}
             style={{ fontSize: 80 }} />
          <span>{label}</span>
        </div>
      </ModalBody>
      {isSuccess ? (<ModalFooter className="justify-content-center">
        <Button color="primary" onClick={this.state.callback}>ตกลง</Button>
      </ModalFooter>) : null}
    </Modal>);
  }
}
