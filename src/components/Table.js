import React from "react";
import { useTable, usePagination, useSortBy } from "react-table";
import classnames from "classnames";
import DatatablePagination from "./DatatablePagination";

const Table = ({ columns, data, divided = false, defaultPageSize = 6, ...rest }) => {

  const {
    getTableProps,
    getTableBodyProps,
    prepareRow,
    headerGroups,
    page,
    canPreviousPage,
    canNextPage,
    pageCount,
    gotoPage,
    setPageSize,
    state: { pageIndex, pageSize }
  } = useTable({
      columns, data, initialState: { pageIndex: 0, pageSize: rest.noPage ? 1000 : defaultPageSize }
    },
    useSortBy,
    usePagination
  );

  return (
    <>
      <table {...getTableProps()} className={`r-table table ${classnames({ "table-divided": divided })}`}>
        <thead>
        {headerGroups.map((headerGroup) => (
          <tr {...headerGroup.getHeaderGroupProps()}>
            {headerGroup.headers.map((column, columnIndex) => (
              <th key={`th_${columnIndex}`}
                  {...column.getHeaderProps(column.getSortByToggleProps())}
                  className={
                    column.isSorted
                      ? column.isSortedDesc
                      ? "sorted-desc"
                      : "sorted-asc"
                      : ""}>
                {column.render("Header")}
                <span />
              </th>
            ))}
          </tr>
        ))}
        </thead>
        <tbody {...getTableBodyProps()}>
        {page.map((row) => {
          prepareRow(row);
          return (
            <tr {...row.getRowProps()}>
              {row.cells.map((cell, cellIndex) => (
                <td
                  key={`td_${cellIndex}`}
                  {...cell.getCellProps({
                    className: cell.column.cellClass
                  })}                >
                  {cell.render("Cell")}
                </td>
              ))}
            </tr>);
        })}
        </tbody>
      </table>
      <DatatablePagination
        page={pageIndex}
        pages={pageCount}
        canPrevious={canPreviousPage}
        canNext={canNextPage}
        pageSizeOptions={[4, 10, 20, 30, 40, 50]}
        showPageSizeOptions={false}
        showPageJump={false}
        defaultPageSize={pageSize}
        onPageChange={(p) => gotoPage(p)}
        onPageSizeChange={(s) => setPageSize(s)}
        paginationMaxSize={pageCount}
        noPage={rest.noPage}
        {...rest}
      />
    </>
  );
};

export default Table;
