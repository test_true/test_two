import {
  FOODS_LIST_SEARCH,
  FOODS_SELECT_SEARCH,
  FOODS_SELECT_UPDATE
} from "../actions";
import foods from "../../data/food.json";
import foodsMin from "../../data/foodMin.json";

const INIT_STATE = {
  selected: [],
  // selected: foodsMin,
  rawData: foods
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case FOODS_LIST_SEARCH:
      console.log(state)
      return {
        ...state
      };
    case FOODS_SELECT_SEARCH:
      return {
        ...state
      };
    case FOODS_SELECT_UPDATE:
      return {
        ...state,
        selected: action.payload.foods
      };
    default:
      return { ...state };
  }
};
