import {
  FOODS_LIST_SEARCH,
  FOODS_SELECT_SEARCH,
  FOODS_SELECT_UPDATE
} from "../actions";

export const searchFoods = () => ({
  type: FOODS_LIST_SEARCH
});
export const searchSelectFoods = () => ({
  type: FOODS_SELECT_SEARCH
});
export const updateSelectFoods = (foods) => ({
  type: FOODS_SELECT_UPDATE,
  payload: { foods }
});
