import { combineReducers } from 'redux';
import authUser from './auth/reducer';
import foods from "../redux/foods/reducer";

const reducers = combineReducers({
  authUser,
  foods
});

export default reducers;
