/* AUTH */
export const LOGIN_USER = 'LOGIN_USER';

/* FOODS */
export const FOODS_LIST_SEARCH = 'FOODS_LIST_SEARCH';
export const FOODS_SELECT_SEARCH = 'FOODS_SELECT_SEARCH';
export const FOODS_SELECT_UPDATE = 'FOODS_SELECT_UPDATE';

export * from './auth/actions';
export * from './foods/actions';
