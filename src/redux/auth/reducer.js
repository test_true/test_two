import {
  LOGIN_USER
} from "../actions";

const INIT_STATE = {
  fullname: "",
  age: null
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case LOGIN_USER:
      return { user: action.payload.user };
    default:
      return { ...state };
  }
};
