import { all, fork, takeEvery } from "redux-saga/effects";
import {
  LOGIN_USER
} from "../actions";

export function* watchLoginUser() {
  yield takeEvery(LOGIN_USER, loginWithEmailPassword);
}
function* loginWithEmailPassword({ payload }) {
  const { history } = payload;
  history.push("/app");
}
export default function* rootSaga() {
  yield all([
    fork(watchLoginUser)
  ]);
}
