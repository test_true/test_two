import React from "react";

export const buildColumns = (header, jsonKey, width, customCell = undefined, customCellClass = "cell-table") => ({
  Header: header, accessor: jsonKey, cellClass: `list-item-heading w-${width} ${customCellClass}`, // eslint-disable-next-line react/react-in-jsx-scope
  Cell: (props) => (customCell ? customCell(props) : <div className="data-table-value">{props.value}</div>)
});

const formatCurrency = ({ value }) => {
  return Number(value).toLocaleString(undefined, {
    // minimumFractionDigits: 2,
    maximumFractionDigits: 3
  });
};

export const cellUtils = {
  formatCurrency
};
