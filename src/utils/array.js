const mergeArray = (data) => {
  if (data) {
    const result = {};

    data.forEach(basket => {
      for (let [key, value] of Object.entries(basket)) {
        if (result[key] && typeof result[key] === "number") {
          result[key] += value;
        } else {
          result[key] = value;
        }
      }
    });
    return result;
  }
  return [];
};

export { mergeArray };
