import React from "react";
import { NavLink } from "react-router-dom";
import { adminRoot } from "../../constants/defaultValues";

const TopNav = () => {
  return (
    <nav className="navbar fixed-top">

      <NavLink className="navbar-logo" to={adminRoot}>
        <h2 className="text-primary d-none d-xs-block">
          Test No.2
        </h2>
      </NavLink>

    </nav>
  );
};

export default TopNav;
