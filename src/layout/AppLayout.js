import React from "react";
import { withRouter } from "react-router-dom";

import TopNav from "../containers/navs/Topnav";

const AppLayout = ({ children, history }) => {
  return (
    <div id="app-container">
      <TopNav history={history} />
      <main>
        <div >{children}</div>
      </main>
    </div>
  );
};
export default withRouter(AppLayout);
