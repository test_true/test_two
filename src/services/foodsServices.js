import axios from "axios";

const authenticationUrl = process.env.REACT_APP_API_URL;
const foodsAxios = axios.create({
  baseURL: authenticationUrl
});

/*foodsAxios.interceptors.request.use((config) => {
  config.headers.Authorization = `Basic xxx`;
  config.headers["Accept-Language"] = "th";
  return config;
});*/

export default {
  login: async (username, password) => {
    const url = `/login`;
    return foodsAxios.post(url, { username, password });
  }, sendFoods: async (params) => {
    const url = `/sendFoods`;
    const result = await foodsAxios.post(url, params);
    const { status, message } = result.data;
    return { status, message };
  }
};
